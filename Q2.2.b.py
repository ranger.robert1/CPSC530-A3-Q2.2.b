
#Generates a mac table given a macGen function
def genMacTable(macGen):
    
    P=""
    for i in range(0,5):
        P += "m = " + str(i) + "\t"
    P= "\t\t" + P + "\n"
    k = 0
    tab = [[0 for x in range(5)]  for y in range(625)]
    for u0 in range (0,5):
        for u1 in range (0,5):
            for u2 in range(0,5):
                for u3 in range(0,5):
                #Generating keys row description
                    P += "k" + str(k) + "=(" + str(u0) + "," + str(u1) + ")(" + str(u2) + "," + str(u3) + ")" + "\t"
                    for m in range(0,5):
                        c = macGen(m, u0, u1, u2, u3)
                        P +=  str(c) + "\t"
                        tab[k][m] = c
                    k += 1
                    P += '\n' 
    return tab

#Counts the possible keys for each message tag pair
def countKeys(tab):
    #S[m][t]
    S = [[0 for t in range(5)]  for m in range(5)]
    for m in range(0,5):
        for t in range(0,5):
            for k in range(625):
                if (tab[k][m] == t):
                    S[m][t] += 1
    return S

def countKeys3(tab):
    #S[m][t]
    S = [[0 for t in range(25)]  for m in range(5)]
    Tags = ["00","01","02","03","04",
            "10","11","12","13","14",
            "20","21","22","23","24",
            "30","31","32","33","34",
            "40","41","42","43","44",]
    for m in range(0,5):
        for t in range(0,25):
            for k in range(625):
                if (tab[k][m] == Tags[t]):
                    S[m][t] += 1
    return S

#Count the quantity of each (m,t)
def countOcurrences3(tab):
    count = [[0 for i in range(25)] for x in range(5)]
    for m in range(5):
        for k in range(625):
            if (tab[k][m] == "00"):
                count[m][0] += 1
            elif (tab[k][m] == "01"):
                count[m][1] += 1
            elif (tab[k][m] == "02"):
                count[m][2] += 1
            elif (tab[k][m] == "03"):
                count[m][3] += 1
            elif (tab[k][m] == "04"):
                count[m][4] += 1
            elif (tab[k][m] == "10"):
                count[m][5] += 1
            elif (tab[k][m] == "11"):
                count[m][6] += 1
            elif (tab[k][m] == "12"):
                count[m][7] += 1
            elif (tab[k][m] == "13"):
                count[m][8] += 1
            elif (tab[k][m] == "14"):
                count[m][9] += 1
            elif (tab[k][m] == "20"):
                count[m][10] += 1
            elif (tab[k][m] == "21"):
                count[m][11] += 1
            elif (tab[k][m] == "22"):
                count[m][12] += 1
            elif (tab[k][m] == "23"):
                count[m][13] += 1
            elif (tab[k][m] == "24"):
                count[m][14] += 1
            elif (tab[k][m] == "30"):
                count[m][15] += 1
            elif (tab[k][m] == "31"):
                count[m][16] += 1
            elif (tab[k][m] == "32"):
                count[m][17] += 1
            elif (tab[k][m] == "33"):
                count[m][18] += 1
            elif (tab[k][m] == "34"):
                count[m][19] += 1
            elif (tab[k][m] == "40"):
                count[m][20] += 1
            elif (tab[k][m] == "41"):
                count[m][21] += 1
            elif (tab[k][m] == "42"):
                count[m][22] += 1
            elif (tab[k][m] == "43"):
                count[m][23] += 1
            elif (tab[k][m] == "44"):
                count[m][24] += 1
    return count


def countOcurrences(tab):
    count = [[0 for i in range(5)] for x in range(5)]
    for m in range(5):
        for k in range(625):
            if (tab[k][m] == 0):
                count[m][0] += 1
            elif (tab[k][m] == 1):
                count[m][1] += 1
            elif (tab[k][m] == 2):
                count[m][2] += 1
            elif (tab[k][m] == 3):
                count[m][3] += 1
            elif (tab[k][m] == 4):
                count[m][4] += 1
    return count

#MAC functions
def MAC(m,u0,u1):
    return ((m*u1 + u0) % 5)

def genM1(m,u0,u1,u2,u3):
    return (MAC(m,u0,u1) + MAC(m,u2,u3)) % 5

def genM2(m,u0,u1,u2,u3):
    return (MAC(m,u0,u1) * MAC(m,u2,u3)) % 5

def genM3(m,u0,u1,u2,u3):
    return str(MAC(m,u0,u1)) + str(MAC(m,u2,u3)) 

m1 = genMacTable(genM1)
m2 = genMacTable(genM2)
m3 = genMacTable(genM3)
print("Message tag pairs in m1:")
print(countOcurrences(m1))
print("Message tag pairs in m2:")
print(countOcurrences(m2))
print("Message tag pairs in m3:")
print(countOcurrences3(m3))

print("Quantity of possible keys for each (m,t) in  m1:")
print(countKeys(m1))
print("Quantity of possible keys for each (m,t) in  m2:")
print(countKeys(m2))
print("Quantity of possible keys for each (m,t) in  m3:")
print(countKeys3(m3))

